using Microsoft.AspNetCore.Mvc;
using Serilog;

namespace TestWebapi;


[ApiController]
public class TestController: ControllerBase {

    private readonly ILogger<TestController> _logger;


    public TestController(ILogger<TestController> logger)
    {
        _logger = logger;
    }


    [HttpGet("api/v1/good-endpoint")]
    public IActionResult GetGoodControllerEndpoint()
    {
        Log.Information("This is an information log message");
        return Ok("Working");
    }

    [HttpGet("api/v1/bad-endpoint")]
    public IActionResult GetBadControllerEndpoint()
    {
        Log.Error("This is an error log message");
        return BadRequest("Not working");
    }

}